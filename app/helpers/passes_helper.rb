module PassesHelper

  def new_pass_date pass
    date = pass.pass_date.present? ? 
              pass.pass_date.strftime("%m/%d/%Y") : 
              DateTime.now.in_time_zone('Central Time (US & Canada)').strftime("%m/%d/%Y") 
    "<input class='datepicker form-control input-sm' value='#{date}' type='text' name='pass[pass_date]' />".html_safe
  end

  def generate_checkbox model, attr
    if model.send(attr).present?
      model.send(attr).in_time_zone('Central Time (US & Canada)').strftime("%l:%M %p")
    else
      "<input type='checkbox' name='#{attr}' data-id=#{model.id} />".html_safe
    end
  end

end
