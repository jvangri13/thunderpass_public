class AddFieldsToSchool < ActiveRecord::Migration
  def change
    add_column  :schools, :periods, :text
    add_column  :schools, :destinations, :text
  end
end
