class User < ActiveRecord::Base
  has_many :authorizations, dependent: :delete_all
  has_many :passes        , dependent: :delete_all
  belongs_to :school

  validates :email, uniqueness: {scope: :school}, presence: true
  
end
