class WelcomeController < ApplicationController
  before_filter :must_be_logged_in, only: :new
  def login
    redirect_to home_path if current_user
  end

  def new
  end

  
end
