class Pass < ActiveRecord::Base
  belongs_to :user
  belongs_to :school

  scope :for_my_school, ->(current_user){where(school_id: current_user.user.school_id)}
  scope :between_dates, ->(from, to){where ("pass_date between '#{from}' and '#{to}' ")}
  scope :closed,        ->{where ("arrived_at is null or departed_at is null")}
  
  validates :student_first_name , presence: true
  validates :student_last_name  , presence: true
  validates :destination        , presence: true
  validates :user_id            , presence: true
  validates :school_id          , presence: true
  validates :period             , presence: true
  validates :pass_date          , presence: true


  def student_whole_name
    "#{self.student_first_name} #{student_last_name}"
  end

end
