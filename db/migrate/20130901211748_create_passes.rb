class CreatePasses < ActiveRecord::Migration
  def change
    create_table :passes do |t|
      t.string      :task
      t.datetime    :pass_date
      t.string      :destination
      t.string      :student_first_name
      t.string      :student_last_name
      t.string      :period
      t.datetime    :departed_at
      t.datetime    :arrived_at
      t.integer     :user_id
      t.integer     :school_id
      t.timestamps
    end

    add_index :passes,  :pass_date
    add_index :passes,  :student_last_name
    add_index :passes,  :destination
    add_index :passes,  :school_id
  end
end
