class AddNicknameToSchool < ActiveRecord::Migration
  def change
    add_column :schools, :nickname, :string
  end
end
