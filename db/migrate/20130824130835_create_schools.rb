class CreateSchools < ActiveRecord::Migration
  def change
    create_table :schools do |t|
      t.string :name
      t.integer :district_id
      t.string :contact_email
      t.string :contact_name

      t.timestamps
    end
  end
end
