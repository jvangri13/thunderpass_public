class School < ActiveRecord::Base
  belongs_to  :district
  serialize   :periods, Array
  serialize   :destinations, Array

  def squished_name
    "#{name.split(' ').join.downcase}_#{self.id}"
  end

end
