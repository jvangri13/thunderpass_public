set :applicationdir, "<app dir>"
set :rails_env, 'staging'
set :branch, 'staging'
set :deploy_to, applicationdir
set :keep_releases, 5