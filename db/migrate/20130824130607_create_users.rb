class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string  :email
      t.integer :school_id
      t.boolean :admin, default: false

      t.timestamps
    end
  end
end
