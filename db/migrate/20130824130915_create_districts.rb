class CreateDistricts < ActiveRecord::Migration
  def change
    create_table :districts do |t|
      t.string :name
      t.string :contact_email
      t.string :contact_name

      t.timestamps
    end
  end
end
