class CreateAuthorizations < ActiveRecord::Migration
  def change
    create_table :authorizations do |t|
      t.string  :first_name
      t.string  :last_name
      t.string  :provider
      t.string  :uid
      t.integer :user_id

      t.timestamps
    end
  end
end
