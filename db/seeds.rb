district = District.create(name: 'District 200')
school = School.new(name: 'Woodstock North High School', district: district)
school.periods = %w(1 2 3 4/5 4/6 5/6 7 8 9 )
school.destinations = ["Library", "PE", "Guidance", "Office", "Nurse", "Restroom", "Peer Tutor", "Classroom"]
school.save!
user = User.create(email: 'jvangri13@gmail.com', school: school, admin: true)
User.create(email: '<user1>', school: school, admin: true)
User.create(email: '<user2>', school: school, admin: true)
User.create(email: '<user3>', school: school, admin: true)
if Rails.env.development?
  o =  [('a'..'z'),('A'..'Z')].map{|i| i.to_a}.flatten
  (1..250).each do |num|
    arrived_at =    num.days.ago if num % 4 == 0
    departed_at =   num.days.ago if num % 4 == 0 || num % 2 == 0
    first_name  =   (0...6).map{ o[rand(o.length)] }.join
    last_name   =   (0...10).map{ o[rand(o.length)] }.join
    destination =   (0...10).map{ o[rand(o.length)] }.join
    period      =   school.periods[rand(0..8)]
    Pass.create!( school_id: school.id, user_id: user.id, period: period, arrived_at: arrived_at, 
                  departed_at: departed_at, student_first_name: first_name, student_last_name: last_name,
                  pass_date: DateTime.parse('2013-09-23') , destination: destination)
  end
end
  