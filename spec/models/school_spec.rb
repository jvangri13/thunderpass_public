require 'spec_helper'

describe School do
  it "should save periods" do 
    periods = %w(1 2 3 4/5 4/6 5/6 7 8 9 )
    school = FactoryGirl.create(:school)
    school.periods = periods
    school.save
    school.periods.first.should eql "1"
    school.periods.last.should eql "9"
  end
  it "should save destinations" do 
    destinations = %w(Library PE Guidance Office Nurse Restroom)
    school = FactoryGirl.create(:school)
    school.destinations = destinations
    school.save
    school.destinations.first.should eql "Library"
    school.destinations.last.should eql "Restroom"
  end
end
