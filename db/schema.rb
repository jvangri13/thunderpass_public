# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20140921132519) do

  create_table "authorizations", force: true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "districts", force: true do |t|
    t.string   "name"
    t.string   "contact_email"
    t.string   "contact_name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "passes", force: true do |t|
    t.string   "task"
    t.datetime "pass_date"
    t.string   "destination"
    t.string   "student_first_name"
    t.string   "student_last_name"
    t.string   "period"
    t.datetime "departed_at"
    t.datetime "arrived_at"
    t.integer  "user_id"
    t.integer  "school_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "passes", ["destination"], name: "index_passes_on_destination", using: :btree
  add_index "passes", ["pass_date"], name: "index_passes_on_pass_date", using: :btree
  add_index "passes", ["school_id"], name: "index_passes_on_school_id", using: :btree
  add_index "passes", ["student_last_name"], name: "index_passes_on_student_last_name", using: :btree

  create_table "schools", force: true do |t|
    t.string   "name"
    t.integer  "district_id"
    t.string   "contact_email"
    t.string   "contact_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "periods"
    t.text     "destinations"
    t.string   "nickname"
    t.string   "domain"
  end

  create_table "users", force: true do |t|
    t.string   "email"
    t.integer  "school_id"
    t.boolean  "admin",      default: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

end
