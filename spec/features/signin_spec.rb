require 'spec_helper'

describe "the signin process", :type => :feature do

  it "should redirect to root url and show an error" do
    set_invalid_omniauth
    visit root_url
    click_link 'Google'
    expect(page).to have_content 'Warning! No user was found with that email address.
    Are you signed into another google account? Try signing out here: Sign out.
    Welcome to ThunderPass Sign in with: Google'
  end

  it "should log me in when the user is found and an Authorization record exists" do
    set_omniauth
    user = double(User)
    user.stub(:email).and_return("test@some_test_domain.com")
    user.stub(:id).and_return(1)

    auth = double(Authorization)
    auth.stub(:user_id).and_return(user.id)
    auth.stub(:first_name).and_return("Jeff")
    auth.stub(:last_name).and_return("Van")

    Authorization.stub_chain(:where, :first).and_return(auth)
    User.stub_chain(:where, :first).and_return(user)

    visit root_url
    click_link 'Google'
    expect(page).to have_content 'Welcome Jeff, you are now signed in!'
  end

  it "should log me in and create an Authorization record when the user is found and no Authorization record exists" do
    set_omniauth
    user = double(User)
    user.stub(:email).and_return("test@some_test_domain.com")
    user.stub(:id).and_return(1)
    User.stub_chain(:where, :first).and_return(user)

    visit root_url
    click_link 'Google'
    expect(page).to have_content 'Welcome Good, you are now signed in!'
    Authorization.count.should eql 1
  end

end

private 

def set_omniauth
  OmniAuth.config.add_mock(
    :google_oauth2, 
      {
        :uid => '123',
        :info => {
          :email => 'test@some_test_domain.com',
          :first_name =>'Good',
          :last_name => 'User'
      }
  })
  OmniAuth.config.test_mode = true
end

def set_invalid_omniauth
  OmniAuth.config.add_mock(
    :google_oauth2, 
      {
         :uid => 'xxx', 
        :info => {
          :email => 'test@some_test_domain.com',
          :first_name =>'Invalid',
          :last_name => 'User'
      }
  })
  OmniAuth.config.test_mode = true
end 