class PassesController < ApplicationController
  before_filter :must_be_logged_in  

  def index
    @header_lambda = ->(stuff){passes_path(stuff)}
    @passes = Pass.for_my_school(current_user).closed.where(pass_date: DateTime.now.strftime("%Y-%m-%d"))
                      .includes(:user => :authorizations)
                      .order(order_by_string)
  end

  def new
    @pass = Pass.new
  end

  def create
    @pass = Pass.new(pass_params.merge({
                      pass_date: DateTime.strptime(params[:pass][:pass_date], "%m/%d/%Y"),
                      user_id: current_user.user_id, 
                      school_id: current_user.user.school_id}))
    if @pass.save
      redirect_to passes_path, notice: "Pass was successfully created"
    else
      render new_pass_path
    end
  end

  def update    
    pass = Pass.find(params[:id])
    if pass
      pass.update_attribute(params[:attr_name], params[:attr_value] === "true" ? Time.now : nil)
    end

    formatted_departed_at  = pass[:departed_at].in_time_zone('Central Time (US & Canada)')
                              .strftime("%l:%M %p") unless pass[:departed_at].blank?
    formatted_arrived_at   = pass[:arrived_at].in_time_zone('Central Time (US & Canada)')
                              .strftime("%l:%M %p") unless pass[:arrived_at].blank?
    respond_to do |format|
      format.json do 
        flash[:alert] = "You just updated the #{params[:attr_name].humanize}
                          for #{CGI.escapeHTML(pass.student_whole_name)}. To undo this
                          click here: #{undo_link(pass, params[:attr_name])}".html_safe
        render json: pass.attributes.merge({departed_at: formatted_departed_at, 
                                            arrived_at: formatted_arrived_at})
      end
      format.html do
        redirect_to passes_path(order_by: params[:order_by], asc_desc: params[:asc_desc])
      end
    end
  end

  def destroy
    pass = Pass.find(params[:id])    
    if pass
      pass.destroy
      redirect_to passes_path
    end
  end

  def report
    @header_lambda = ->(stuff){report_path(stuff)}
    @passes = Pass.for_my_school(current_user)
                  .between_dates(report_params[:from_date], report_params[:to_date])
                  .order(order_by_string)
                  .page(params[:page]).per(20)
  end
  
private 

  def undo_link pass, attr_name
    order_by = params[:order_by].present? ? params[:order_by] : "updated_at"
    asc_desc = params[:asc_desc].present? ? params[:asc_desc] : "desc"
    ActionController::Base.helpers.link_to('undo', 
      pass_path(pass.id, attr_name: attr_name, attr_value: "", 
                order_by: order_by, asc_desc: asc_desc),
                method: 'PUT')
  end

  def order_by_string
    order_by = params[:order_by] || 'period'
    asc_desc = params[:asc_desc] || 'asc'
    order_by_str = order_by === "student_last_name" ?  
                              "#{order_by}" " #{asc_desc}" :
                              "#{order_by} #{asc_desc}, student_last_name asc"
    order_by_str
  end

  def report_params
    from_date = params[:from_date].present? ? format_date(params[:from_date]) : DateTime.now
    to_date = params[:to_date].present? ? format_date(params[:to_date]) : DateTime.now

    {from_date: from_date, to_date: to_date}
  end


  def pass_params
    params.require(:pass).permit(:user_id, :school_id, :period, :student_first_name, 
                                  :student_last_name, :task, :destination, :pass_date)
  end

  def format_date date
    DateTime.strptime(date, "%m/%d/%Y").strftime("%Y-%m-%d")
  end

end