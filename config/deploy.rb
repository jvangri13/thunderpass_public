require "rvm/capistrano"
require 'bundler/capistrano'
require 'capistrano/ext/multistage'

set :stages, %w(production staging)
set :default_stage, "staging"
set :user, <user>
set :domain, <domain>
set :port, <port>
 
set :scm, 'git'
set :repository, "git@bitbucket.org:jvangri13/thunderpass_public.git"
set :git_shallow_clone, 1
set :scm_verbose, true
 
role :web, domain
role :app, domain
role :db,  domain, :primary => true
 
set :deploy_via, :remote_cache

default_run_options[:pty] = true  
ssh_options[:keys] = %w(/home/user/.ssh/id_rsa)
set :ssh_options, {:forward_agent => true}

# Passenger
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end
# end