class Authorization < ActiveRecord::Base
  belongs_to :user

  validates :user_id  , presence: true
  validates :provider , presence: true
  validates :uid      , uniqueness: {scope: [:provider, :user]}, presence: true


  def self.find_or_create_from_auth_hash(auth, user_id)
    user = Authorization.where(provider: auth["provider"], uid: auth["uid"], user_id: user_id).first || 
      Authorization.create_with_omniauth(auth, user_id)
  end

  def self.create_with_omniauth(auth, user_id)
    create! do |new_auth|
      new_auth.provider = auth["provider"]
      new_auth.uid = auth["uid"]
      new_auth.first_name = auth["info"]["first_name"]
      new_auth.last_name = auth["info"]["last_name"]
      new_auth.user_id = user_id
    end
  end

end
