set :applicationdir, "<app dir>"
set :rails_env, 'production'
set :branch, 'master'
set :deploy_to, applicationdir
set :keep_releases, 4