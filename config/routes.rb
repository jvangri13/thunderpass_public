Thunderpass::Application.routes.draw do
  root 'welcome#login'

  get "/auth/:provider/callback"  => "sessions#create_third_party", :as => :auth_callback
  get "/signout"                  => "sessions#destroy",            :as => :signout
  get "/signout_google"           => "sessions#signout_google",     :as => :signout_google
  get '/home'                     => 'welcome#new',                 :as => :home
  
  get  "/passes/report"             =>  "passes#report",              :as => :report
  resources :passes 
  resources :users
 end
