module ApplicationHelper

  def header_name &path
    asc_desc, arrow_class = determine_arrow_class('student_last_name')
    ("<th>" + link_to("Name", path.call(params.merge(order_by: :student_last_name, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def header_destination &path
    asc_desc, arrow_class = determine_arrow_class('destination')
    ("<th>" + link_to("Destination", path.call(params.merge(order_by: :destination, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def header_period &path
    asc_desc, arrow_class = determine_arrow_class('period')
    ("<th>" + link_to("Period", path.call(params.merge(order_by: :period, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def header_date &path
    asc_desc, arrow_class = determine_arrow_class('pass_date')
    ("<th>" + link_to("Date", path.call(params.merge(order_by: :pass_date, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def header_departed_at &path
    asc_desc, arrow_class = determine_arrow_class('departed_at')
    ("<th>" + link_to("Departed", path.call(params.merge(order_by: :departed_at, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def header_arrived_at &path
    asc_desc, arrow_class = determine_arrow_class('arrived_at')
    ("<th>" + link_to("Arrived", path.call(params.merge(order_by: :arrived_at, asc_desc: asc_desc)), {class: arrow_class}) + "</th>").html_safe
  end

  def determine_arrow_class header_name
    arrow_class = ""
    if params[:order_by] == header_name
      arrow_class = params[:asc_desc] == "asc"? 'up_arrow' : 'down_arrow'
    end
    asc_desc = params[:asc_desc] == "asc"? 'desc' : 'asc'
    [asc_desc, arrow_class]
  end

  def school_nickname
    current_user.user.try(:school).nickname
  end
  
end
