class UsersController < ApplicationController
  
  before_filter :must_be_admin

  def index
    @users = User.where(school: current_user.user.school).order(:email)
    @users.reject!{|user| user.email === current_user.user.email}
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attribute(:email, params[:user][:email])
      redirect_to users_path, notice: "#{@user.email} was updated successfully."
    end
  end

  def create
    @user = User.new(email: params[:user][:email], school: current_user.user.school, admin: false)

    if @user.save
      redirect_to users_path, notice: "#{@user.email} was created successfully."
    else
      render new_user_path
    end
  end

  def destroy
    if user = User.find(params[:id])
      user.destroy
      redirect_to users_path
    end
  end

end
