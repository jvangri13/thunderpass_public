class SessionsController < ApplicationController

  def create_third_party
    auth = request.env["omniauth.auth"]
    host = request.host
    user = User.joins(:school).where(email: auth[:info][:email], schools: {domain: host}).first
    if user 
      session[:user_id] = user.id
      Authorization.find_or_create_from_auth_hash(auth, session[:user_id])
      redirect_to home_path
    else 
      redirect_to root_url,   
        :alert => "No user was found with that email address. Are you signed into another google account? Try 
        signing out here: #{ActionController::Base.helpers.link_to("Sign out.", google_signout_url)}".html_safe
    end
  end

  def destroy
    session[:user_id] = nil
    redirect_to root_url
  end

  def signout_google
    session[:user_id] = nil
    redirect_to google_signout_url
  end

end
