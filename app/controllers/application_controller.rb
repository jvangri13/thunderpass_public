class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  helper_method :current_user, :google_signout_url, :report_params

  private

  def current_user
    @current_user ||= Authorization.where(user_id: session[:user_id]).first if session[:user_id]
  end
  
  def google_signout_url
    "https://www.google.com/accounts/Logout?continue=https://appengine.google.com/_ah/logout?continue=http://#{request.host_with_port}"
  end

  def must_be_logged_in
    redirect_to root_url, alert: 'You must be logged in to perform that action.' unless current_user
  end

  def must_be_admin
    redirect_to root_url, alert: 'You must be an admin to perform that action.' unless current_user && current_user.user.admin?
  end

end
