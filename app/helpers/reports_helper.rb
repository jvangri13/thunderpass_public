module ReportsHelper

  def format_date_for_display date
    if date.is_a?(String)
      hash = DateTime._strptime(date, "%Y-%m-%d")
      "#{hash[:mon]}/#{hash[:mday]}/#{hash[:year]}"
    else
      date.strftime("%m/%d/%Y")
    end
  end

end